export class Document {
  constructor(
    public id?: number,
    public codigo?: string,
    public titulo?: string,
    public direccion?:string,
    public autor?: string,
    public estado?: string,
  ){}
}