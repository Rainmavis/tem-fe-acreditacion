import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DomainService {

  private domain: string ="http://192.168.43.31:8000/"
  //private domain: string ="http://34.216.148.9:443/"
 
  
   constructor() { }
 
   getDomain():string{
 
     return this.domain
   }
 
 }