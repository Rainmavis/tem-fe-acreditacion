import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomainService } from './domain.service';
import { Document} from './../models/document'

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  //TO DO: aumentar auth services en el constructor
  constructor( private http: HttpClient, private dom:DomainService) { }


  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
    //Authorization: 'TOKEN '+this.authService.getToken()
  })

  // parte Sin dialog opcion 2:

  getDocuments() {
    const url_api = this.dom.getDomain()+'api/documento/get/select/'
    return this.http.get(url_api, {headers: this.headers});
  }

  addDocument(doc: Document) {
    const url_api = this.dom.getDomain()+'api/documento/set/insert/'
    return this.http.post(url_api,doc, { headers: this.headers });
  }
  updateDocument( document: Document) {
    const url_api = this.dom.getDomain()+`api/documento/set/update/${document.id}/`
    return this.http.put(url_api, document, { headers: this.headers });
  }
  deleteDocument(id: number) {
    const url_api = this.dom.getDomain()+`api/documento/set/delete/${id}/`
    return this.http.delete(url_api, { headers: this.headers });
  }

  // COMPARTIENDO METODOS

}
