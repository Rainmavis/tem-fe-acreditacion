import { MasterService } from './../../../services/master.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'ms-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: ['./document-form.component.scss']
})
export class DocumentFormComponent implements OnInit {

  public form: FormGroup
  public id:any =''
  
  types: any []
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<DocumentFormComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = this.fb.group({
      id: null,
      codigo: ['',[Validators.required]],
      titulo: ['',[Validators.required]],
      direccion: ['',[Validators.required]],
      autor: ['',[Validators.required]],
      estado: '1'
    })
  }

  ngOnInit() {
    this.initForm()
  }

  onCancel(): void {
    this.dialogRef.close('Cancel')
  }

  initForm() {
    if (this.data == null) {
      this.id=null
      
    }
    else {
      console.log("actualizare",this.data)
      this.id=this.data.id
      this.form.setValue(this.data)
    }
  }

  onSubmit(data:any) {
    if(this.form.valid){
      console.log(data)
      this.dialogRef.close(data)
      this.form.reset()
    }
  }
  onClose(){
    this.dialogRef.close('Close')
    this.form.reset()
  }

}